### About

Simple FreeBSD SLiM theme inspired on the FBSD SLiM theme.

### Preview!
![SLiM FreeBSD Black Theme](https://bytebucket.org/rigoletto-freebsd/slim-freebsd-black-theme/raw/962413abb6abe7622bdfd554bde1591547254ee7/src/preview.png)

### Installation
```shell
git clone https://bitbucket.org/rigoletto-freebsd/slim-freebsd-black-theme.git
cp -R slim-freebsd-black-theme/src /usr/local/share/slim/themes/slim-freebsd-black-theme
```

- Open up `/usr/local/etc/slim.conf` file and set
  `slim-freebsd-black-theme` as your current theme.

```shell
# current theme, use comma separated list to specify a set to 
# randomly choose from
current_theme	slim-freebsd-black-theme
```

### Configuration
- The theme uses the Montserrat font by default, but you can change it
  editing the
  `/usr/local/share/slim/themes/slim-freebsd-black-theme/slim.theme` file
  and setting the desired font.
